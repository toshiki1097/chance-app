class User < ApplicationRecord
    has_many :microposts, dependent: :destroy
    
    before_save { self.email = email.downcase }
    
    VALID_PASS_REGEX = /\A(?=.*\d)(?=.*\w)[A-Za-z\d][\w]+\z/
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    VALID_ID_REGEX = /\A[A-Za-z\d][\w]+\z/
    validates :id, length: { in: 4..20 }, uniqueness: true, format: { with: VALID_ID_REGEX }
    validates :name, presence: true
    validates :email, presence: true, uniqueness: true, format: { with: VALID_EMAIL_REGEX }
    validates :password, length: { in: 8..25 }, format: { with: VALID_PASS_REGEX }
    
    has_secure_password
    
end
