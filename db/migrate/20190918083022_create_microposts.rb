class CreateMicroposts < ActiveRecord::Migration[5.1]
  def change
    create_table :microposts do |t|
      t.text :content
      t.string :user_id
    end
    add_foreign_key :microposts, :users
    add_index  :microposts, :user_id
  end
end
