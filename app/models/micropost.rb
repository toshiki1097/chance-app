class Micropost < ApplicationRecord
    belongs_to :user, inverse_of: :microposts
    validates :content, length: { maximum: 1000 }, presence: true
end
